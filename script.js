// //Basics
// console.table({ fname: "arun", age: 21 });
// console.error("sample error");
// console.warn("sample warning");
// console.clear();
// console.log([50.3, 30, 30]);
// //json value key,pairs
// console.log({ fname: "arun", age: 21 });
// // we can calculate time for a function
// console.time("timer");
// for (i = 0; i < 100; i++) {
//   console.log(i);
// }
// console.timeEnd("timer");
// const a = 45;
// console.log(a);
// a = 30;
// console.log(a);
// const student = { name: "arun", age: 21 };
// console.log(student);
// console.log(student.name);
// student.name = "pandiyan";
// console.log(student.name);

// //data types

// var z = 25;
// console.log(typeof z);
// var nm = "arun";
// console.log(typeof nm);
// var bool = true;
// console.log(typeof bool);
// var n = null;
// console.log(typeof n);
// var ud;
// console.log(typeof ud);
// var s1 = Symbol();
// console.log(typeof s1);
// var s2 = Symbol();
// console.log(typeof s2);

// console.log(s1 == s2);

// var arr = [3, 3, 3, 3];
// console.log(typeof arr);

// var obj = {
//   name: "arun",
//   age: "21",
// };
// console.log(typeof obj);

// var d = new Date();
// console.log(typeof d);

// //type conversion

// var a = 25;
// console.log(a, typeof a);
// a = String(a);
// console.log(a, typeof a);

// var a = 25.5;
// console.log(a, typeof a);
// a = String(a);
// console.log(a, typeof a);

// var a = true;
// console.log(a, typeof a);
// a = String(a);
// console.log(a, typeof a);

// var a = new Date();
// console.log(a, typeof a);
// a = String(a);
// console.log(a, typeof a);

// var a = [1, 2, 3, 4, 5];
// console.log(a, typeof a);
// a = String(a);
// console.log(a, typeof a);

// var a = 25;
// console.log(a, typeof a);
// a = a.toString();
// console.log(a, typeof a);

a = [1, 2, 3, 4, 5];
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);

a = true;
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);

a = false;
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);

a = "1";
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);

a = "1.5";
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);

a = "1";
console.log(a, typeof a);
a = parseInt(a);
console.log(a, typeof a);

a = "1.5";
console.log(a, typeof a);
a = parseInt(a);
console.log(a, typeof a);

a = "1.5";
console.log(a, typeof a);
a = parseFloat(a);
console.log(a, typeof a);

a = "arun";
console.log(a, typeof a);
a = Number(a);
console.log(a, typeof a);
